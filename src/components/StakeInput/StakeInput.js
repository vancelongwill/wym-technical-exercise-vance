import React from 'react'
import styled from 'styled-components'

const StyledInput = styled.input`
  appearance: none;
  box-shadow: none;
  border: none;
  border-radius: 5px;
  padding: 5px;
  font-size: 16px;
  margin-top: 5px;
  margin-bottom: 5px;
`

export function StakeInput(props) {
  return(    
    <StyledInput type='text' onChange={(e) => {
      props.updateStake(Number(e.target.value))
    }} value={props.stake} />
  ) 
}
